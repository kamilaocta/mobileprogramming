import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, Button } from 'react-native';

export default class Biodata extends Component{
    render(){
        return(
            <View style = {styles.container}>
                <View style={{marginTop:64, alignItems:'center'}}>
                    <View style = {styles.avatarcontainer}>
                        <Image style={styles.avatar} source = {require('./assets/kamilaa.jpg')} />
                    </View>
                    <Text style={styles.name}>Kamila Octaviani</Text>
                </View>
                <View style={styles.statsContainer}>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>4</Text>
                        <Text style={styles.statTitle}>Post</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>905</Text>
                        <Text style={styles.statTitle}>Followers</Text>
                    </View>
                    <View style={styles.stat}>
                        <Text style={styles.statAmount}>300</Text>
                        <Text style={styles.statTitle}>Following</Text>
                    </View>
                </View>
                <View style={styles.status}>
                    <Text style={styles.study}>Program studi : Teknik Informatika </Text>
                </View>
                <View style={styles.class}>
                    <Text style={styles.kelas}>Kelas : Pagi A </Text>
                </View>
                <View style={styles.instagram}>
                    <Text style={styles.ig}>Instagram : @kmlaoct_ </Text>
                   
                </View>
                <View style={styles.facebook}>
                    <Text style={styles.fb}>Facebook : Kamila Octaviani</Text>
                </View>
            </View>
        )
    }
} 
const styles = StyleSheet.create({
    container:{
        backgroundColor: 'gray',
        flex:1,
        width: 400
    },
    avatarcontainer: {
        shadowColor:'#151734',
        shadowRadius: 15,
        shadowOpacity: 0.4
    },
    avatar: {
        width: 136,
        height: 136,
        borderRadius: 68
    },
    name: {
        color: 'black',
        marginTop: 24,
        fontSize: 20,
        fontWeight: 'bold'
    },
    statsContainer: {
        flexDirection: "row",
        justifyContent: 'space-between',
        margin: 50,
        bottom: 20
    },
    stat: {
        alignItems: 'center',
        flex : 1
    },
    statAmount:{
        color:'#4F566D',
        fontSize: 18,
        fontWeight: 'bold'
    },
    statTitle: {
        fontSize: 16,
        color: 'black',
        fontWeight: '300',
        marginTop: 4
    },
    status: {
        bottom: 20,
        left: 60
    },
    study:{
        justifyContent: 'flex-end',
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
    },
    class: {
        bottom:-20,
        left: 60
    },
    kelas:{
        fontWeight: 'bold',
        fontSize : 25
    },
    instagram: {
        bottom : -50,
        left : 60
    },
    ig: {
        fontSize: 25,
        fontWeight: 'bold'
    },
    facebook:{
        bottom: -90,
        left : 60
    },
    fb: {
        fontWeight :'bold',
        fontSize: 25
    },
    state : {
        fontSize : 25,
        left : 60,
        bottom :-15
    }
});