//Conditional
//1. if-else
var nama = ""
var peran = ""
if (nama == ""){
    console.log("Nama harus diisi!")
} else if (nama == "John" || peran == ""){
    console.log("Halo John, pilih peranmu untuk memulai game!")
} else if (nama == "Jane" || peran == "Penyihir"){
    console.log("Selamat datang di Werewolf, Jane!")
    console.log("Halo penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf")
} else if (nama == "Jenita" || peran == "Guard"){
    console.log("Selamat datang di dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf")
} else if (nama == "Junaedi" || peran == "Werewolf"){
    console.log("Selamat datang di dunia Werewolf, Junaedi")
    console.log("Halo Werewolof Junaedi, kamu akan memakan memangsa setiap malam!")
} else {
    console.log("Game berakhir!")
}


//2.switch case
var hari = 12
var bulan = 10
var tahun = 2000

if(hari <=31 || bulan <=12 || tahun <=1900 || tahun >=2200)
{
    switch (bulan)
    {
        case 1 : 
            bulan = 'Januari'
            break;
        case 2 : 
            bulan = 'Februari'
            break;
        case 3 : 
            bulan = 'Maret'
            break;
        case 4 : 
            bulan = 'April'
            break;
        case 5 : 
            bulan = 'Mei'
            break;
        case 6 : 
            bulan = 'Juni'
            break;
        case 7 : 
            bulan = 'Juli'
            break;
        case 8 : 
            bulan = 'Agustus'
            break;
        case 9 : 
            bulan = 'September'
            break;
        case 10 : 
            bulan = 'Oktober'
            break;
        case 11 : 
            bulan = 'November'
            break;
        case 12 : 
            bulan = 'Desember'
            break;
        default :
        bulan = ('Tidak ada bulan')
    }
    console.log(hari+' '+bulan+' '+tahun)
}